WorkTracker

This application will help me to learn AngularJS. It will be used to track daily tasks performed at work. It will save tasks performed in a day and will be used to report time spent on them. 

It will communicate via Web APIs to a data store of some kind.