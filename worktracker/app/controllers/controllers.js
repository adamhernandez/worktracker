﻿app.controller("WorkdayController", function ($scope, workdayService) {

    $scope.addWorkDay = function (workday) {
        workdayService.addWorkDay(workday);
    }
});

app.controller("PreviousController", function ($scope, workdayService) {

    $scope.workdays = workdayService.getWorkdays();
    $scope.getWorkdaysByContains = function (value) {
        return workdayService.getWorkdaysByContains(value);
    }

    $scope.getWorkdaysByDates = function (begin, end) {
        return workdayService.getWorkdaysByDates(begin, end)
    }

    $scope.getWorkdays = function () {
        return $scope.workdays;
    }
});

app.controller("StatsController", function ($scope, workdayService) {
    $scope.getWorkdaysByContains = function (value) {
        return workdayService.getStatsByContains(value);
    }

    $scope.getWorkdaysByDates = function (begin, end) {
        return workdayService.getStatsByDates(begin, end)
    }
});

app.controller('NavbarController', function ($scope, $location) {
    $scope.getClass = function (path) {
        if ($location.path().substr(0, path.length) == path) {
            return true
        } else {
            return false;
        }
    }
});