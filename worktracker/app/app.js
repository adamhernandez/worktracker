﻿/// <reference path="../Scripts/angular.js" />

var app = angular.module("workTrackerApp", ["ngRoute"]);

app.config(function ($routeProvider) {
    $routeProvider
        .when("/workday", {
            controller: "WorkdayController",
            templateUrl: "app/views/workday.html"
        })
        .when("/previous", {
            controller: "PreviousController",
            templateUrl: "app/views/previous.html"
        })
        .when("/stats", {
            controller: "StatsController",
            templateUrl: "app/views/stats.html"
        })
        .otherwise({
            redirectTo: "/workday"
        });
});