﻿app.service("workdayService", function () {
    var workdays = [
        {
            taskId: 0,
            begin: new Date(2015, 3, 23, 4, 0, 0, 0),
            end: new Date(2015, 3, 23, 6, 0, 0, 0),
            name: "task 1",
            hours: 3,
            notes: "task notes",
            day: new Date("03/23/2015"),
        },
        {
            taskId: 1,
            begin: new Date(2015, 3, 23, 4, 0, 0, 0),
            end: new Date(2015, 3, 23, 6, 0, 0, 0),
            name: "task 2",
            hours: 3,
            notes: "task notes",
            day: new Date("03/23/2015"),
        },
        {
            taskId: 2,
            begin: new Date(2015, 3, 23, 4, 0, 0, 0),
            end: new Date(2015, 3, 23, 6, 0, 0, 0),
            name: "task 3",
            hours: 3,
            notes: "task notes",
            day: new Date("03/23/2015"),
        }
    ];

    this.getWorkdaysByContains = function (value) {
        return workdays;
    }

    this.getWorkdaysByDates = function (begin, end) {
        return workdays;
    }

    this.getStatsByContains = function (value) {
        return workdays;
    }

    this.getStatsByDates = function (begin, end) {
        return workdays;
    }

    this.getWorkdays = function () {
        return workdays;
    }

    this.updateWorkday = function (workday) {
        // implement me
    }

    this.addWorkDay = function (workday) {
        workdays.push(workday);
    }
});