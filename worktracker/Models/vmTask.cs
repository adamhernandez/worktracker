﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace worktracker.Models
{
    public class vmTask
    {
        public int TaskId { get; set; }
        public string Name { get; set; }
        public double Hours { get; set; }
        public string Notes { get; set; }
    }
}
