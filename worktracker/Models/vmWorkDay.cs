﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace worktracker.Models
{
    public class vmWorkDay
    {
        public int WorkDayId { get; set; }
        public DateTime? Day { get; set; }
        public DateTime? Begin { get; set; }
        public DateTime? End { get; set; }
        public IEnumerable<vmTask> Tasks { get; set; }
    }
}